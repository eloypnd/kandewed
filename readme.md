#kandewed

Online "invitation" for Kamila and Eloy weeding.

Built with:

- [Meteor](https://www.meteor.com/)
- and [Meteorite](http://oortcloud.github.io/meteorite/) package manager with the following packages:
    - [bootstrap3-less](https://github.com/eprochasson/bootstrap3-less) Bootstrap 3 for Meteor, with Less
    - bootstrap3-wysiwyg
    - [meteor-headers](https://github.com/gadicohen/meteor-headers) Access HTTP headers on both server and client. Client IP with proxy support.
    - [iron-touter](https://github.com/EventedMind/iron-router) A client and server side router designed specifically for Meteor.
    - [jquery-easing](https://github.com/kenyee/meteor-jquery-easing) Repackaging of jquery.easing plugin for Meteor.js
    - [momentjs](https://github.com/crapthings/meteor-momentjs) A javascript date library for parsing, validating, manipulating, and formatting dates.
    - [roles](https://github.com/alanning/meteor-roles) Role-based authorization, compatible with Meteor's built-in accounts packages.  Includes example app, unit tests and online API docs.
    - blaze-layout
    - GAnalytics
    - jquery-easing
    - mailchimp
    - inject-initial
    - underscore

**Work in progress...**

# instructions to deploy app

### first time instalation

1. go to [mongolab](http://mongolab.com) and create database and user
2. create heroku app
    `heroku create <heroku_app_name> --stack cedar --region eu --buildpack https://github.com/eloypineda/heroku-buildpack-meteorite.git`
3. add git remote
   `git remote add <remote_name> git@heroku.com:<heroku_app_name>.git`
4. setup env variables (MONGO_URL, ROOT_URL, METEOR_SETTINGS and MAIL_URL)
    - `heroku config:set MONGO_URL='mongodb://<username>:<password>@<mongolag_url>/<dbname>' --app <heroku_app_name>`
    - `heroku config:set ROOT_URL='http://<heroku_app_name>.herokuapp.com/' --app <heroku_app_name>`
    - `heroku config:set METEOR_SETTINGS='{"defaults":{"roles":["admin","editor","guest"]},"services":{"logentries":{"token":"<logentries_token>"},"facebook":{"appId":"<appId>","secret":"<secret>"}},"public":{"ga":{"account":"<UA-00000000-0>"}}}' --app <heroku_app_name>`
    - `heroku config:set MAIL_URL='smtp://<username>:<password>@smtp.mandrillapp.com:587'`
5. push repo to heroku:
    `git push <remote_name> <branch_name>:master`

If created a custom domain don't forget to update the ROOT_URL
    `heroku config:set ROOT_URL='http://<custom_domain>/' --app <heroku_app_name>`

  Enable [lab feature on heroku for websockets](https://devcenter.heroku.com/articles/heroku-labs-websockets):
    `heroku labs:enable websockets -a <heroku_app_name>`

### deploy

5. push repo to heroku
    `git push <remote_name> <branch_name>:master`

### reference

- [Deploying Meteor app to heroku with Meteorite and MongoLab](https://coderwall.com/p/gurjmw)
- [How to deploy Meteor on Heroku with external MongoDB](http://ondrej-kvasnovsky.blogspot.de/2013/05/how-to-deploy-meteor-on-heroku-with.html)
- [heroku dev center : reference](https://devcenter.heroku.com/categories/reference)
    - [Deploying with Git](https://devcenter.heroku.com/articles/git)
    - [Managing Multiple Environments for an App](https://devcenter.heroku.com/articles/multiple-environments)
    - [Custom Domains](https://devcenter.heroku.com/articles/custom-domains)
- [mongolab : docs and support](http://docs.mongolab.com/)
    - [Backup and Recovery : Using mongodump/mongorestore](http://docs.mongolab.com/backups/#dump-and-restore)

####Credits
---
* [FULLSCREEN BACKGROUND IMAGE SLIDESHOW WITH CSS3](http://tympanus.net/codrops/2012/01/02/fullscreen-background-image-slideshow-with-css3/)
* Loadin spinner from [Loading...](http://jxnblk.github.io/loading/)
* [Bootbox.js](http://bootboxjs.com/) programmatic dialog boxes using Twitter’s Bootstrap modals
