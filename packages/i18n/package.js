Package.describe({
  summary: "kandewed i18n package"
});

Package.on_use(function (api, where) {
  api.use(['ui', 'headers']);
  api.add_files('i18n.js', ['client', 'server']);
  api.export('i18n');
});
