i18n = {

  translations: [],
  locale: 'en-US',

  t: function (str) {
    if(i18n.translations[this.language] && i18n.translations[this.language][str]){
      return i18n.translations[this.language][str];
    }
    return str;
  },

  /**
   * initialize i18n internationalization
   */
  initialize: function () {
    this.setClientLocale();
    this.setLang();
  },

  /**
   * language getter
   *
   * @return {string} language
   */
  getLang: function () {
    return this.language;
  },

  /**
   * locale getter
   *
   * @return {string} language
   */
  getLocale: function () {
    return this.locale;
  },

  setLang: function () {
    // for now we just get the 2 first characters of the locale
    this.language = this.locale.substring(0, 2);
  },

  /**
   * set/modify the Languange
   */
  setLocale: function (lang) {
    lang = typeof lang !== 'undefined' ?  lang : 'en-US';
    this.locale = lang;
    this.setLang();
    localStorage.setItem('Meteor.locale', lang);
  },

  /**
   * set the Language in the client
   */
  setClientLocale: function () {
    if (sessionLang = localStorage.getItem('Meteor.locale')) {
      this.locale = sessionLang;
    } else {
      var userLocale = this.getUserLocale();
      localStorage.setItem('Meteor.locale', userLocale);
      this.locale = userLocale;
    }
  },

  /**
   * find user language preferences
   *
   * 1. if user logged and have locale
   * 2. get locale from accept-language http header
   */
  getUserLocale: function () {
    if (Meteor.user() && Meteor.user().profile.locale) {
      // (1) if user logged and have locale setting we get this one
      return Meteor.user().profile.locale;
    } else if (acceptLangHeader = headers.get('accept-language')) {
      // (2) we check http accept-language header
      return this.parseAcceptLangHeader(acceptLangHeader);
    } else {
      // (3) check browser language throught 'navigator.language' object
      if (typeof navigator.language !== 'undefined') {
        // for all standard browsers
        return navigator.language;
      } else if (typeof navigator.browserLanguage !== 'undefined') {
        // for our dear friend IE
        return navigator.browserLanguage;
      }
    }
  },

  /**
   * parse http accept-language header
   * @param  {string} acceptLang accept-language http header
   * @return {string}            preferred languange (es, en, pl, ...)
   */
  parseAcceptLangHeader: function (acceptLang) {
    acceptLang = typeof acceptLang !== 'undefined' ? acceptLang : '';

    // TODO simplify: get just locale codes and get first one
    var extractRegions = /(([a-zA-Z][a-zA-Z-]+),?)*(;q=)(([0-9]*.)?[0-9]*)/g;
    var regions = acceptLang.match(extractRegions);

    // for string with single locale code (like es-ES)
    if (!regions && typeof acceptLang === 'string' && acceptLang.length < 6) {
      return acceptLang;
    }

    var results = {
      preferred: {
        lang: '',
        rate: 0
      }
    };

    regions.forEach(function (region) {
      var extractLang = /([a-zA-Z][a-zA-Z-]+)/g;
      var extractRate = /q=(([0-9]*.)?[0-9]*)/;

      var langs = region.match(extractLang);
      var rate = Number(region.match(extractRate)[1]);

      if (typeof langs === 'string' && typeof rate === 'string') {
        results[lang] = rate;
      } else {
        langs.forEach(function (lang) {
          results[lang] = rate;
          if (rate > results.preferred.rate) {
            results.preferred = {
              lang: lang,
              rate: rate
            };
          }
        })
      }
    });

    return results.preferred.lang;
  }

};

if(Meteor.isClient){
  UI.registerHelper('i18n', function(str){
    return i18n.t(str);
  });
}
