// TODO: make posible to log with the server logentries client
//       or with the client-side one
// LE.init('<token>');
/*
LogentriesAPI = function (token, options) {
  this.le = LE.init({
    token: '<token>',
    print: true
  });
}

LogentriesAPI.prototype.info = function(message) {
  this.le.info(message);
};
*/

logger = {
  log: function (level, message) {
    Meteor.call('logen', level, message);
  },
  debug: function (message) {
    Meteor.call('logen', 'debug', message);
  },
  info: function (message) {
    Meteor.call('logen', 'info', message);
  },
  notice: function (message) {
    Meteor.call('logen', 'notice', message);
  },
  warning: function (message) {
    Meteor.call('logen', 'warning', message);
  },
  error: function (message) {
    Meteor.call('logen', 'err', message);
  },
  crit: function (message) {
    Meteor.call('logen', 'crit', message);
  },
  alert: function (message) {
    Meteor.call('logen', 'alert', message);
  },
  emerg: function (message) {
    Meteor.call('logen', 'emerg', message);
  },
  route: function () {
    var path = document.location.pathname,
        params = document.location.search,
        hash = document.location.hash,
        lang = i18n.getLocale(),
        user_agent = navigator.userAgent,
        clientIP = 'none';
    // workaround to fix issue in headers package
    try {
      clientIP = headers.getClientIP();
    }
    catch (e) {
      clientIP = 'error getting IP';
      Meteor.call('logen', 'err', e.stack);
    }
    var message = clientIP + ' - "GET ' + path + ' '+hash+params+'" - "' + lang +'" - "'+user_agent+'"';

    /**
     * logging full headers to verbose
     *
     * TODO: set logging param to choose more or less verbose
     *
    var message = {
      path: location.pathname,
      language: i18n.getLang().toUpperCase(),
      headers: headers.get()
    };
     */

    // google analytics
    GAnalytics.pageview(path+params);

    Meteor.call('logen', 'info', message);
  }
};
