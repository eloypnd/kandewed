Package.describe("logentries package");

Package.on_use(function (api) {
  if(api.export)
    api.export('logger');
  api.add_files(['logentries_server.js'], 'server');
  api.add_files(['le.js', 'logentries_client.js'], 'client');
});

Npm.depends({
  "node-logentries": "0.1.3"
});
