var logentries = Npm.require("node-logentries");

LogentriesAPI = function (token, options) {
  var me = this;

  if (Meteor.settings.services) {
    this.le = logentries.logger({
      token: Meteor.settings.services.logentries.token,
      printerror: true
    });
  } else {
    this.le = {
      log: function (level, message) {
        me.console(level, message);
      }
    };
    me.warning('There is no token for logentries');
    this.printLogs = true;
  }
}

LogentriesAPI.prototype.log = function(level, message) {
  if (this.printLogs) {
    this.console(level, message);
  }
  this.le.log(level, message);
};
LogentriesAPI.prototype.debug = function (message) {
  this.le.log('debug', message);
}
LogentriesAPI.prototype.info = function (message) {
  this.le.log('info', message);
}
LogentriesAPI.prototype.notice = function (message) {
  this.le.log('notice', message);
}
LogentriesAPI.prototype.warning = function (message) {
  this.le.log('warning', message);
}
LogentriesAPI.prototype.err = function (message) {
  this.le.log('err', message);
}
LogentriesAPI.prototype.crit = function (message) {
  this.le.log('crit', message);
}
LogentriesAPI.prototype.alert = function (message) {
  this.le.log('alert', message);
}
LogentriesAPI.prototype.emerg = function (message) {
  this.le.log('emerg', message);
}
LogentriesAPI.prototype.console = function(level, message) {
  //cmessage = '[' + level + '] >> ' + JSON.stringify(message);
  cmessage = '[' + level + '] >> ' + message;
  console.log(cmessage);
};

logger = new LogentriesAPI();

/**
 * Method to log from the client side
 * @param  {string} level    log level
 * @param  {string|object} message  message to log
 */
Meteor.methods({
  logen: function (level, message) {
    logger.log(level, message);
  }
});
