i18n.translations.pl = {
  // >> general <<
  "kande": "kande",
  "Kamila & Eloy": "Kamila i Eloy",
  "Wedding of Kamila and Eloy": "Wesele Kamili i Eloya",
  "Wedding": "Wesele",
  "Please log in...": "Zaloguj się...",
  "Welcome to": "Witamy na",
  "Website for Kamila and Eloy wedding": "To jest strona ślubna Kamili i Eloya",
  "Krakow": "O Nas",
  "Loading...": "Ładowanie...",

  // >> header <<
  "What happens when the two most magical cities in the world meet?":
    "Co się może zdarzyć kiedy spotkają się dwa najbardziej magiczne miasta?",
  "Saturday, 12 July 2014": "Sobota, 12-stego lipca 2014",

  // >> forms <<

  // login
  "email": "email",
  "Enter email": "Wpisz adres email",
  "password": "hasło",
  "Enter Password": "Wprowadź hasło",
  "Remember me": "Zapamiętaj mnie",
  "Login": "Login",
  "Create account": "Załóż konto",
  "Forgot my password": "Nie pamiętam hasła",
  "Login with facebook": "Zaloguj się przez Facebooka",
  // sign in
  "I have an account, I want to login.": "Mam konto, chcę się zalogować.",
  "Name": "nazwa",
  "Enter name": "Wpisz nazwę",
  "Surname": "nazwisko",
  "Enter surname": "Wpisz nazwisko",
  "Sign in": "Zarejestruj się",
  "Email already exists.": "E-mail już istnieje.",
  "Internal server error": "Ups! Coś nie działa. Spróbuj jeszcze raz za chwilę. Jeśli znów nie działa, daj nam znać <a href=\"mailto:wed@kande.es\">wed@kande.es</a>",

  // >> emails <<
  "verification_email_subject": "Witaj na stronie weselnej Kamili i Eloya!"
};
