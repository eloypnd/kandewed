i18n.translations.en = {
  // >> general <<
  "kande": "kande",
  "Kamila & Eloy": "Kamila & Eloy",
  "Wedding of Kamila and Eloy": "Wedding of Kamila and Eloy",
  "Please log in...": "Please log in...",
  "Welcome to": "Welcome to",
  "Website for Kamila and Eloy wedding": "Website for Kamila and Eloy wedding",

  // >> header <<
  "What happens when the two most magical cities in the world meet?":
    "What happens when the two most magical cities in the world meet?",
  "Saturday, 12 July 2014": "Saturday, 12 July 2014",

  // >> forms <<

  // login
  "email": "email",
  "Enter email": "Enter email",
  "password": "password",
  "Enter Password": "Enter Password",
  "Remember me": "Remember me",
  "Login": "Login",
  "Create account": "Create account",
  "Forgot my password": "Forgot my password",
  "Login with facebook": "Login with facebook",
  // sign in
  "I have an account, I want to login.": "I have an account, I want to login.",
  "Name": "Name",
  "Enter name": "Enter name",
  "Surname": "Surname",
  "Enter surname": "Enter surname",
  "Sign in": "Sign in",
  "Internal server error": "Ups! Something unexpected happens! Try again later or write us an email to <a href=\"mailto:wed@kande.es\">wed@kande.es</a>",

  // >> emails <<
  "verification_email_subject": "Welcome to Kamila and Eloy wedding experience!"
};
