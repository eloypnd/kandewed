i18n.translations.es = {
  // >> general <<
  "kande": "kande",
  "Kamila & Eloy": "Kamila y Eloy",
  "Wedding of Kamila and Eloy": "Boda de Kamila y Eloy",
  "Wedding": "Boda",
  "Please log in...": "Regístrese por favor...",
  "Welcome to": "Bienvenidos a",
  "Website for Kamila and Eloy wedding": "Estás en la página web de la boda de Kamila y Eloy",
  "Krakow": "Cracovia",
  "Loading...": "Cargando...",

  // >> header <<
  "What happens when the two most magical cities in the world meet?":
    "¿Qué pasa cuando las dos ciudades más mágicas del mundo se encuentran?",
  "Saturday, 12 July 2014": "Sábado, 12 de Julio de 2014",

  // >> forms <<

  // login
  "email": "email",
  "Enter email": "Introduzca su email",
  "password": "contraseña",
  "Enter Password": "Introduzca su contraseña",
  "Remember me": "No cerrar sesión",
  "Login": "Entrar",
  "Create account": "Nueva cuenta",
  "Forgot my password": "Olvidé mi contraseña",
  "Login with facebook": "Entrar con facebook",
  // sign in
  "I have an account, I want to login.": "Ya tengo una cuenta de usuario",
  "Name": "Nombre",
  "Enter name": "Introduzca su nombre",
  "Surname": "Apellidos",
  "Enter surname": "Introduzca sus apellidos",
  "Sign in": "Registrarme",
  "Email already exists.": "Ya existe un usuario con este email.",
  "Internal server error": "Ups! Algo ha fallado! Inténtalo de nuevo en un rato y si el error persiste envíanos un emial a <a href=\"mailto:wed@kande.es\">wed@kande.es</a>.",

  // >> emails <<
  "verification_email_subject": "¡Bienvenidos a la web de la boda de Kamila y Eloy!"

};
