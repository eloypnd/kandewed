/**
 * [validate description]
 * @type {Object}
 */
validate = {

  /**
   * validate not empty
   * @param  {string} value
   * @return {bool}
   */
  empty: function (value) {
    if (value) {
      return false;
    }
    return true;
  },

  /**
   * alpha string validation
   * allow letters and hyphens
   *
   * @param  {string} value
   * @return {bool}
   */
  text: function (value) {
    if (typeof value === 'undefined') {
      return false;
    }
    var filter = /^([a-z.]+\s{1})*[a-z.]+$/i;
    if (filter.test(value)) {
        return true;
    }
    return false;
  },

  /**
   * email validation
   * @param  {string} email to validate
   * @return {bool}
   */
  email: function (field) {
    var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(field)) {
      return true;
    }
    return false;
  },

  /**
   * username validation
   * allow letters, numbers, and underscores
   *
   * @param  {string} value
   * @return {bool}
   */
  username: function (value) {
    // TODO make max and min length dinamic
    //minLength = typeof minLength !== 'undefined' ?  minLength : 5;
    //maxLength = typeof maxLength !== 'undefined' ?  maxLength : 15;
    if (typeof value === 'undefined') {
      return false;
    }
    var filter = /^[a-z0-9_\-]{3,18}$/i;
    if (filter.test(value)) {
        return true;
    }
    return false;
  },

  /**
   * password validation
   * allow letters, numbers, and underscores
   *
   * @param  {string} value
   * @return {bool}
   */
  password: function (value) {
    // TODO make max and min length dinamic
    //minLength = typeof minLength !== 'undefined' ?  minLength : 6;
    //maxLength = typeof maxLength !== 'undefined' ?  maxLength : 24;
    if (typeof value === 'undefined') {
      return false;
    }
    var filter = /^[\w_\-\.\?\$\&\(\)\+]{6,24}$/i;
    if (filter.test(value)) {
        return true;
    }
    return false;
  },

};

/**
 * Meteor way
 * create "filter" to use with the 'check' function
 */

/**
 * 'check' valid email
 *
 * @param  {string} email
 * @return {boolean}
 */
ValidEmail = Match.Where(function (email) {
  check(email, String);
  var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(email)) {
      return true;
    }
    return false;
});
