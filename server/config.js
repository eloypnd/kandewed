var facebookAppId = (Meteor.settings.services !== undefined) ? Meteor.settings.services.facebook.appId : '';
var facebookSecret = (Meteor.settings.services !== undefined) ? Meteor.settings.services.facebook.secret : '';

// account config
Accounts.config({
  sendVerificationEmail: true
});
// 3rd party apps config
ServiceConfiguration.configurations.remove({
  service: "facebook"
});
ServiceConfiguration.configurations.insert({
  service: "facebook",
  appId: facebookAppId,
  secret: facebookSecret
});

logger.notice('Config with fb app: ' + facebookAppId)
