Meteor.startup(function () {
  Future = Npm.require('fibers/future');

  if (Roles.getAllRoles().count() === 0) {
    logger.info('>>>> Setting up application');
    logger.info('Adding roles...');
    logger.debug(Meteor.settings);
    Meteor.settings.defaults.roles.forEach(function (role) {
      Roles.createRole(role);
    });
  } else {
    logger.info('>>>> kandewed app started');
  }
});
