//TODO: move to a package
Newsletter = {

  init: function (apiKey, listId, options) {
    if (typeof apiKey === 'undefined') return false;
    if (typeof listId === 'undefined') return false;
    options = (typeof options !== 'undefined') ? options : { version: '2.0' };

    this.listId = listId;

    try {
      this.api = new MailChimp(apiKey, options);
    } catch (error) {
      logger.err( error.message );
    }
  },

  lists: function (options, callback) {
    options = (typeof options !== 'undefined') ? options : {};
    this.api.call(
      'lists',
      'list',
      options,
      Meteor.bindEnvironment(callback, function (err) { logger.err(err); throw (err); })
    );
  },
  subscribe: function (options, callback) {
    options = (typeof options !== 'undefined') ? options : {};
    options.id = this.listId;
    this.api.call(
      'lists',
      'subscribe',
      options,
      Meteor.bindEnvironment(callback, function (err) { logger.err(err); throw (err); })
    );
  },
  memberInfo: function (options, callback) {
    options = (typeof options !== 'undefined') ? options : {};
    options.id = this.listId;
    this.api.call(
      'lists',
      'member-info',
      options,
      Meteor.bindEnvironment(callback, function (err) { logger.err(err); throw (err); })
    );
  }
}

if (Meteor.settings.services) {
  Newsletter.init(
    Meteor.settings.services.mailchimp.apiKey,
    Meteor.settings.services.mailchimp.listId
  );
}

Meteor.methods({
  newsletterLists: function (options) {
    var fut = new Future();

    Newsletter.lists(options, function (error, result) {
      if (error) {
        fut.return(error);
      } else {
        fut.return(result.data);
      }
    });
    return fut.wait();
  },
  newsletterSubscribe: function (options) {
    var fut = new Future();

    Newsletter.subscribe(options, function (error, result) {
      if (error) {
        fut.return(error);
      } else {
        fut.return(result);
      }
    });
    return fut.wait();
  },
  newsletterMemberInfo: function (options) {
    var fut = new Future();

    Newsletter.memberInfo(options, function (error, result) {
      if (error) {
        logger.err(error.message);
        fut.return(error);
      } else {
        fut.return(result);
      }
    });
    return fut.wait();
  }
});
