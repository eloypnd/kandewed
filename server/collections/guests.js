Meteor.methods({
  createGuest: function (options) {
    if ( isAdminById(Meteor.userId()) ) {
      var user = (typeof options.user === 'undefined') ? false : options.user;
      var guest = (typeof options.guest === 'undefined') ? false : options.guest;

      if (user) {
        // create guest from existing user
        var guest = new Guest(user);
        guest.save();
        return guest._id;
      }

    }
  },
  guestSubscribeNewsletter: function (options) {
    // required fields to subscribe to newsletter
    if (typeof options.email.email === 'undefined')
      throw new Meteor.Error(400, 'Valid email address required to subscribe to newsletter');
    var fut = new Future();
    var guest = new Guest();

    // check that guest with email exits
    if (guest.fetch({'profile.email': options.email.email})) {
      var hash = Random.id();

      options.merge_vars.HASH = hash;
      Newsletter.subscribe(options,  function (error, result) {
        if (error) {
          logger.err('[Mailchimp]: '+error);
          fut.throw(new Meteor.Error(error.code, error.message));
        } else {
          logger.info('[Mailchimp]: '+result);
          var doc = {
            newsletter: {
              mailchimp: result,
              hash: hash
            }
          };
          guest.update(guest._id, {$set: doc});
          fut.return(result);
        }
      });
      return fut.wait();
    }
  }
});


/*
 * class Guest
 *
 * @param options [object User]
 */
function Guest (options) {
  options = (typeof options === 'undefined') ? false : options;
  this.data = {};

  if (options) {
    // relation between user and guest
    if (typeof options._id !== 'undefined') {
      this.data.userId = options._id;
    }
    // copy profile data
    if (typeof options.profile !== 'undefined') {
      this.data.profile = options.profile;
      delete this.data.profile.createdAt;
      delete this.data.profile.facebook;
    }
    // copy facebook data
    if (typeof options.services.facebook !== 'undefined') {
      this.data.facebook = options.services.facebook;
      delete this.data.facebook.accessToken;
      delete this.data.facebook.expiresAt;
    }
    this.data.createdAt = this.data.modifiedAt = new Date();
  }
}

Guest.prototype.save = function() {
  var me = this;
  if (typeof me._id === 'undefined') {
    me.create();
  } else {
    me.update();
  }
};

Guest.prototype.create = function() {
  var me = this;
  this._id = Guests.insert(me.data, function (error, guestId) {
    if (error) {
      // log error if something goes wrong
      logger.info('Error: ['+error.reason+'] while creating guest ['+guestId+'] by user ['+Meteor.userId()+']');
      logger.err(error);
    } else {
      // log message for new created guest
      logger.info('Guest ['+guestId+'] successfuly created by user ['+Meteor.userId()+']');
      // if the guest have been created from an user
      if (me.data.userId) {
        // update user with new guestId
        Meteor.users.update(
          me.data.userId,
          {$set: {'profile.guestId': guestId}},
          function (error) {
            if (error) {
              logger.info('Error: ['+error.reason+'] while updating user with new guest ID ['+guestId+'] by user ['+Meteor.userId()+']');
              logger.err(error);
            }
          }
        );
        // update user with new role
        Roles.addUsersToRoles(me.data.userId, 'weddingGuest');
      }
    }
  });
};

Guest.prototype.update = function(selector, modifier) {
  selector = (typeof selector === 'undefined') ? {_id: this._id} : selector;
  modifier = (typeof modifier === 'undefined') ? this.data : modifier;

  var me = this;

  Guests.update(selector, modifier, function (error, ndocs) {
    if (error) {
      //logger.info('Error: ['+error.reason+'] while updating guest ['+me._id+'] by user ['+Meteor.userId()+']');
      logger.err('[Mongo] ' + error);
    } else {
      logger.info('Guest ['+me._id+'] successfuly updated by user ['+Meteor.userId()+']');
    }
  });
};

Guest.prototype.fetch = function(options) {
  if (!_.isEmpty(this.data)) return false;
  var doc = {};

  if(doc = Guests.findOne(options)) {
    this.data = doc;
    this._id = doc._id;
    return doc._id;
  } else {
    return false;
  }
};
