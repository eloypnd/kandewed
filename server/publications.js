Meteor.publish('allUsers', function() {
  if (Roles.userIsInRole(this.userId, 'admin')) {
    return Meteor.users.find({});
  }
  return [];
});

Meteor.publish('allGuests', function() {
  if (Roles.userIsInRole(this.userId, 'admin')) {
    return Guests.find({});
  }
  return [];
});
Meteor.publish('singleGuest', function(id) {
  return Guests.find(id);
});

Meteor.publish('allContent', function () {
  if (Roles.userIsInRole(this.userId, 'admin')) {
    return Content.find({});
  }
  return [];
});

Meteor.publish('singleContent', function(id) {
  return Content.find(id);
});

Meteor.publish('pageContent', function (page) {
  return Content.find({$or: [{page: page}, {page: 'any'}]});
});
