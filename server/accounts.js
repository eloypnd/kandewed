// Create new users
Accounts.onCreateUser(function(options, user){
  var profile = {},
      //TODO: move to Guest
      guest = { userId: user._id };

  if (typeof user.services.facebook !== "undefined") {
    profile = _.pick(user.services.facebook,
      'name', 'first_name', 'last_name', 'gender', 'locale', 'email'
    );
    //TODO: move to Guest
    guest.profile = profile;
    guest.facebook = user.services.facebook;
    profile.facebook = {
      id: user.services.facebook.id,
      username: user.services.facebook.username
    }
  } else if (typeof user.emails !== "undefined") {
    profile = {
      name: options.name,
      first_name: options.first_name,
      last_name: options.last_name,
      email: options.email,
      locale: i18n.getLocale()
    };
    //TODO: move to Guest
    guest.profile = profile;
  } else {
    profile = { name: 'esteusuarionodeberiadeexistir' };
  }
  profile.createdAt = user.createdAt;

  user.profile = profile;

  user.roles = ['guest'];


  // Subscribe new user to newsletter
  // I need to check how to handle async and future here
  // In the meanwhile I will subscribe the user to the newsletter in the client
  /*
  Newsletter.subscribe(
    {
      email: {email: user.profile.email},
      merge_vars: {
        EMAIL: user.profile.email,
        FNAME: user.profile.first_name,
        LNAME: user.profile.last_name,
        mc_language: user.profile.locale.substring(0, 2)
      }
    },
    function (error, result) {
      if (error) {
        logger.err('[Mailchimp]: '+error);
        fut.throw(error);
      }
      var doc = {
        newsletter: {
          mailchimp: result,
          hash: hash
        }
    };
    guest.update(guest._id, {$set: doc});
    fut.return(result);
  });
  */

  // create guest from existing user
  //TODO: move to Guest
  guest.createdAt = guest.modifiedAt = new Date();
  guest._id = Guests.insert(guest, function (error, guestId) {
    if (error) {
      // log error if something goes wrong
      logger.info('Error: ['+error.reason+'] while creating guest ['+guestId+'] on creating user ['+user._id+']');
      logger.err(error);
    } else {
      // log message for new created guest
      logger.info('Guest ['+guestId+'] successfuly created for user ['+user._id+']');
    }
  });
  if (typeof guest._id !== 'undefined') {
    user.roles.push('weddingGuest');
    user.profile.guestId = guest._id;
  }

  console.log(guest);

  console.log(user);
  logger.notice(user);
  return user;
});

// Meteor acconut emails
Accounts.emailTemplates.siteName = "kandewed";
Accounts.emailTemplates.from = "Kamila & Eloy <wed@kande.es>";

Accounts.emailTemplates.verifyEmail = {
   subject: function(user) {
      return i18n.t("Witamy / Bienvenidos / Welcome");
   },
   text: function(user, url) {
       var greeting = (user.profile && user.profile.name) ?
           ("Czesc / Hola / Hello " + user.profile.name + ",") : "Czesc / Hola / Hello,";
       return greeting + "\n"
              + "\n"
              + "Teraz musimy zweryfikowac Twoj adres e-mail. Kliknij na poniższy link.\n"
              + "\n"
              + "Verifica tu email por favor. Haz click en el enlace que encontrarás al final.\n"
              + "\n"
              + "Now we need you to verify your account email. Just click the link below.\n"
              + "\n"
              + url + "\n"
              + "\n"
              + "Dziekuje / Gracias / Thanks.\n";
   },
   html: function(user, url) {
       var greeting = (user.profile && user.profile.name) ?
           ("Czesc / Hola / Hello " + user.profile.name + ",") : "Czesc / Hola / Hello,";
       return greeting + "\n"
              + "\n"
              + "Teraz musimy zweryfikowac Twoj adres e-mail. Kliknij na poniższy link.\n"
              + "\n"
              + "Verifica tu email por favor. Haz click en el enlace que encontrarás al final.\n"
              + "\n"
              + "Now we need you to verify your account email. Just click the link below.\n"
              + "\n"
              + "<a href='" + url + "'>Zweryfikuj swój adres e-mail / Verifica tu email / Verify your email</a>\n"
              + "\n"
              + url + "\n"
              + "\n"
              + "Dziekuje / Gracias / Thanks.\n";
   }
};
