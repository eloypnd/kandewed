Content = new Meteor.Collection("content");

Content.allow({
  insert: function (userId, doc) {
    if (isAdminById(userId)) {
      return true;
    }
    return false;
  },
  update: function (userId, doc, fields, modifier) {
    if (isAdminById(userId)) {
      return true;
    }
    return false;
  },
  remove: function (userId, doc) {
    if (isAdminById(userId)) {
      return true;
    }
    return false;
  }
});

Meteor.methods({
  updateContent: function (contentId, doc) {
    if (isAdminById(Meteor.userId())) {
      Content.update(contentId, {
        $set: doc
      }, function(error, affectedDocs){
        if (error) {
          logger.info('Error: ['+error.reason+'] while editing content ['+contentId+'] by user ['+Meteor.userId()+']');
          logger.error(error);
          throwError(error.reason);
        } else if (affectedDocs == 0) {
        } else {
          logger.info('Content ['+contentId+'] edited successfuly by user ['+Meteor.userId()+']. '+affectedDocs+' docs modified.');
        }
      });
    }
  }
});
