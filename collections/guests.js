Guests = new Meteor.Collection("guests");

Guests.allow({
  insert: function (userId, doc) {
    if (isAdminById(userId)) {
      return true;
    }
    return false;
  },
  update: function (userId, doc, fields, modifier) {
    if (isAdminById(userId)) {
      return true;
    }
    return false;
  },
  remove: function (userId, doc) {
    if (isAdminById(userId)) {
      return true;
    }
    return false;
  }
});
