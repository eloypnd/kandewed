configs.wysihtml5 = {
  html: true,
  parserRules: {
    classes: {
      "kandewed": 1
    },
    tags: {
      strong: {},
      em: {},
      p: {},
      small: {}
    }
  }
}
