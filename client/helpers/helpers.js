UI.registerHelper('cnt', function(hook) {
  var lang = i18n.getLang();
  var result = Content.findOne({hook: hook});
  if (typeof result === 'undefined') {
    return '<img src="loading-spin.svg" width="18" height="18" class="spinner"> &nbsp; &nbsp;' + i18n.t('Loading...');
    // uncoment for debug mode
    //return 'Missing [<i>'+hook+'</i>]. Please add content from your <a href="/admin/content">admin panel</a>.';
  }
  return result.content[lang];
});

// user related helpers
UI.registerHelper('facebook', function (user) {
  if (user === undefined ) return false;
  if (typeof user.profile.facebook !== 'undefined') {
    // users collections
    return user.profile.facebook;
  } else if (typeof user.facebook !== 'undefined') {
    // guests collections
    return user.facebook;
  } else {
    return false;
  }
});

UI.registerHelper('profilePicture', function (user) {
  if (user === undefined ) return false;
  if (typeof user.profile.facebook !== 'undefined') {
    return 'https://graph.facebook.com/' + user.profile.facebook.id + '/picture';
  } else if (typeof user.facebook !== 'undefined') {
    return 'https://graph.facebook.com/' + user.facebook.id + '/picture';
  } else {
    return '/images/profile_default.jpg';
  }
});

UI.registerHelper('editMode', function (section) {
  section = (typeof section === 'undefined') ? false : section;
  var editMode = Session.get('editMode', section);

  if (section === editMode) {
    return true;
  } else {
    return false;
  }
});
