UI.registerHelper('formatDate', function(date) {
  return date ? moment(date).calendar() : false;
});
