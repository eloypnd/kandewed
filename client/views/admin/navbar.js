Template.navbar_login.helpers({
  username: function() {
    // TODO all users may have a profile.name
    if (Meteor.user().profile !== undefined) {
      return Meteor.user().profile.name;
    } else {
      return Meteor.user().emails[0].address;
    }
  },
  profile_img: function() {
    if (Meteor.user().profile.facebook !== undefined) {
      return 'https://graph.facebook.com/' + Meteor.user().profile.facebook.username + '/picture';
    } else {
      return 'profile_default.jpg';
    }
  }
});
Template.navbar_login.events({
  'click #logout': function (e, tmpl) {
    e.preventDefault();

    Meteor.logout();
    }
});
