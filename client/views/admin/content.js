deleteContent = function (e, tmpl) {
  var contentId = e.currentTarget.id;
  if (isAdminById(Meteor.userId())) {
    Content.remove(contentId, function (error) {
      if (error) {
        logger.info('Error: ['+error.reason+'] while deleting content ['+contentId+'] by user ['+Meteor.userId()+']');
        logger.error(error);
      } else {
        logger.info('Content ['+contentId+'] successfuly deleted by user ['+Meteor.userId()+']');
      }
    });
  }
}

Template.content_row.events({
  'click .btn-delete': deleteContent
});
Template.content_edit.events({
  'click .btn-delete': deleteContent
});

Template.content_edit.events({
  'click button.btn-save': function (e, tmpl) {
    var content  = this,
        $title   = tmpl.find('input#title'),
        $page    = tmpl.find('input#page'),
        $content = tmpl.find('textarea#content');

    e.preventDefault();
    if(!Meteor.user()){
      logger.warning('Attempt to create/edit by no logged user.');
      throwError('You must be logged in.');
      return false;
    }

    var doc = {
      name: {
        en: tmpl.find('input#name_en').value,
        pl: tmpl.find('input#name_pl').value,
        es: tmpl.find('input#name_es').value
      },
      content: {
        en: tmpl.find('textarea#content_en').value,
        pl: tmpl.find('textarea#content_pl').value,
        es: tmpl.find('textarea#content_es').value
      },
      hook: tmpl.find('input#hook').value,
      page: tmpl.find('input#page').value
    }

    if (content.contentId === "n") {
      doc.createdAt = new Date();
      doc.modifiedAt = doc.createdAt;
      Content.insert(doc, function (error, contentId) {
        if (error) {
          logger.info('Error: ['+error.reason+'] while creating content ['+contentId+'] by user ['+Meteor.userId()+']');
          logger.error(error);
        } else {
          logger.info('Content ['+contentId+'] successfuly created by user ['+Meteor.userId()+']');
          Router.go('/admin/content/'+contentId+'/edit');
          logger.route();
        }
      });
    } else {
      doc.modifiedAt = new Date();
      //TODO check why Collection.update doesn't work on client side
      Meteor.call('updateContent', content.contentId, doc, function (error) {
        if (error) {
          logger.info('Error: ['+error.reason+'] while editing content ['+content.contentId+'] by user ['+Meteor.userId()+']');
          logger.error(error);
        } else {
          logger.info('Content ['+content.contentId+'] successfuly edited by user ['+Meteor.userId()+']');
        }
      });
    }

  }
});

Template.content_edit.rendered = function (argument) {
  $('textarea').wysihtml5('deepExtend', configs.wysihtml5);
}
