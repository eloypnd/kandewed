Template.users_list.rendered = function () {
  var height = window.innerHeight || document.body.clientHeight;

  this.find('ul.users').style.height = height+'px';
};
Template.users_list.helpers({
  nUsers: function () {
    if (typeof this.users === 'undefined') return 0;

    return this.users.count();
  }
});
Template.users_list.events({
  'keyup .search input': function (e) {
    var query = e.currentTarget.value;

    Session.set('query', query);
  }
});

Template.users_list_row.events({
  'click .user a.name': function (e) {
    $(e.currentTarget.parentNode.parentNode.parentNode.children).removeClass('active');
    $(e.currentTarget.parentNode.parentNode).addClass('active');
    Session.set('selectedUser', this._id);
  }
});
Template.users_list_row.helpers({
  isGuest: function () {
    return (this.profile.guestId !== undefined) ? this.profile.guestId : false;
  },
  isActiveUser: function () {
    return (this._id == Session.get('selectedUser')) ? 'active' : '';
  }
});

Template.user_info.events({
  'click #add-guest': function (e, tmpl) {
    Meteor.call('createGuest',
      { user: this.user },
      function (error, guestId) {
        if (error) {
          logger.info('Error: ['+error.reason+'] while creating guest ['+guestId+'] by user ['+Meteor.userId()+']');
          logger.error(error);
        } else {
          logger.info('Guest ['+guestId+'] successfuly created by user ['+Meteor.userId()+']');
        }
      }
    );
  }
});
Template.user_info.helpers({
  isGuest: function () {
    return (this.user.profile.guestId !== undefined) ? this.user.profile.guestId : false;
  }
});
