var signinEvents = {
  /**
   * submit form to create new user
   */
  'click button': function (e, tmpl) {
    e.preventDefault();

    // disable button and show loadin spinner
    var btnText = e.target.textContent;
    $(e.target).attr('disabled', true);
    $(e.target).html('<img src="loading-spin-white.svg" width="14" height="14"> &nbsp;&nbsp; ' + i18n.t('Loading...'));

    var userValid = true;

    var first_name = tmpl.find('input#first_name');
    if (!validate.text(first_name.value)) {
      first_name.parentNode.className += ' has-error';
      userValid = false;
    }
    var last_name = tmpl.find('input#last_name');
    if (!validate.text(last_name.value)) {
      last_name.parentNode.className += ' has-error';
      userValid = false;
    }
    var email = tmpl.find('input#email');
    if (!validate.email(email.value)) {
      email.parentNode.className += ' has-error';
      userValid = false;
    }
    var password = tmpl.find('input#password');
    if (!validate.password(password.value)) {
      password.parentNode.className += ' has-error';
      userValid = false;
    }
    var name = first_name.value + ' ' + last_name.value;

    if (userValid) {
      Accounts.createUser({
        'name': name,
        'first_name': first_name.value,
        'last_name': last_name.value,
        'email': email.value,
        'password': password.value
      }, function (err) {
        if (err) {
          bootbox.alert(i18n.t(err.reason));
          logger.error(err);
          // enable button and put back button text
          $(e.target).attr('disabled', false);
          $(e.target).html(btnText);
        } else {
          logger.notice('create new user: ' + name);

          // Subscribe new user to newsletter
          Meteor.call('guestSubscribeNewsletter',
            {
              email: {email: email.value},
              merge_vars: {
                EMAIL: email.value,
                FNAME: first_name.value,
                LNAME: last_name.value,
                mc_language: i18n.getLocale()
              },
              double_optin: false
            },
            function (error, result) {
              if (error) {
                logger.error(error);
              } else {
                logger.info('[MAILCHIMP] '+result);
              }
            }
          );

          Router.go('home');
          //TODO not force redirect
          window.location.reload();
        }
      });
    } else {
      // enable button and put back button text
      $(e.target).attr('disabled', false);
      $(e.target).html(btnText);
    }
  },
  /**
   * continue validation on every keyup or focusout event
   */
  'keyup input, focusout input': function (e, tmpl) {
    var inputType = e.currentTarget.attributes.type.value;
    var inputValue = e.currentTarget.value;

    if (!inputValue) {
      return false;
    } else if (validate[inputType](inputValue)) {
      e.currentTarget.parentNode.className = 'form-group col-md-3';
    } else {
      e.currentTarget.parentNode.className += ' has-error';
    }
  }
};

var signinRendered = function () {
  // focus on first field when template is rendered
  this.find('#first_name').focus();
}

Template.signin.events(signinEvents);

Template.signin.rendered = signinRendered;
