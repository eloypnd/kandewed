Template.user_row.helpers({
  createdAtFormatted: function(){
    return this.profile.createdAt ? moment(this.profile.createdAt).calendar() : '–';
  }
});

Template.user_row.events({
  'click .btn-edit': function (e, tmpl) {
    console.log('edit');
    console.log(this);
    console.log(e);
    console.log(tmpl);
    $(tmpl.lastNode).after('<tr><td colspan="6">'+Template.user_general()+'</td></tr>');
    //$(tmpl.lastNode).after(Template.user_general());
    //$(tmpl.lastNode).after(Template.user_row(tmpl.data));
  }
});
