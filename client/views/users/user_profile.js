Template.user_header.helpers({
  username: function () {
    return Meteor.user().profile.name;
  },
  userPicture: function () {
    if (Meteor.user().services.facebook !== undefined) {
      return 'https://graph.facebook.com/' + Meteor.user().services.facebook.username + '/picture?type=large';
    } else {
      return 'profile_default.jpg';
    }
  }
});

Template.user_general.helpers({
  first_name: function () {
    return Meteor.user().profile.first_name;
  },
  last_name: function () {
    return Meteor.user().profile.last_name;
  },
  gender: function () {
    return Meteor.user().profile.gender;
  },
  locale: function () {
    return Meteor.user().profile.locale;
  },
  // TODO helpers for radio, checkboxes and selects
  // generate them and check-select the active one
  userIsMale: function () {
    return (Meteor.user().profile.gender === "male") ? true : false;
  },
  userIsFemale: function () {
    return (Meteor.user().profile.gender === "female") ? true : false;
  },
  // TODO i18n
  localeES: function () {
    return (Meteor.user().profile.locale === "es_ES") ? true : false;
  },
  localePL: function () {
    return (Meteor.user().profile.locale === "pl_PL") ? true : false;
  },
  localeEN: function () {
    return (Meteor.user().profile.locale !== "es_ES" || Meteor.user().profile.locale !== "pl_PL") ? true : false;
  },
});

Template.user_general.events({
  'click #save': function (e, tmpl) {
    e.preventDefault();
    var user_profile = Meteor.user().profile;

    var form_data = {};
    // get input text values
    tmpl.findAll('input[type=text]').forEach(function (input) {
      var key = input.name;
      form_data[input.name] = input.value;
    });
    // get input radio values
    tmpl.findAll('input[type=radio]:checked').forEach(function (radio) {
      var key = radio.name;
      form_data[key] = radio.value;
    });
    // get select values
    tmpl.findAll('select>option:checked').forEach(function (select_option) {
      form_data[select_option.parentNode.name] = select_option.value;
    });
    _.extend(user_profile, form_data);
    user_profile.modifiedAt = new Date();

    Meteor.users.update(Meteor.userId(), {
      $set: {
        profile: user_profile
      }
    });

  }
});
