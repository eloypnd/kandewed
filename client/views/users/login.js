var loginEvents = {
  /**
   * login user with email/password
   */
  'click button': function (e, tmpl) {
    e.preventDefault();
    var email = tmpl.find('input#email').value;
    var password = tmpl.find('input#password').value;

    Meteor.loginWithPassword(email, password, function (err) {
      if (err) {
        bootbox.alert(i18n.t(err.reason));
        logger.error(err);
      } else {
        logger.notice('user ('+email+') logged in');
        Router.go('home');
        // TODO: render properly home page on login and NOT force refresh
        // [issue #2](https://bitbucket.org/eloypineda/kandewed/issue/2/no-force-refresh-after-login)
        window.location.reload();
      }
    });
  },
  /**
   * send email to recover the password
   */
  'click #forgot-psw': function (e,tmpl) {
    var email = tmpl.find('input#email');

    if (email.value) {
      Accounts.forgotPassword({'email': email.value}, function (err) {
        if (err) {
          bootbox.alert(i18n.t(err.reason));
          logger.error(err);
        } else {
          logger.notice('sent email to reset password for ' + email.value);
        }
      });
    } else {
      $(email).parent().addClass('has-error');
      $(email).focus();
    }
  },
  /**
   * login user with facebook
   */
  'click #fb-login': function (e,tmpl) {
    Meteor.loginWithFacebook({
      requestPermissions: ['email', 'rsvp_event']
    }, function (err) {
      if (err) {
        bootbox.alert(i18n.t(err.reason));
        logger.error(err);
      } else {
        logger.notice('user loggedin with facebook')
      }
      Router.go('/');
      // TODO: render properly home page on login and NOT force refresh
      // [issue #2](https://bitbucket.org/eloypineda/kandewed/issue/2/no-force-refresh-after-login)
      window.location.reload();
    });
  }
};

var loginRendered = function () {
  // focus on first field when template is rendered
  this.find('#email').focus();
}

Template.login.events(loginEvents);

Template.login.rendered = loginRendered;
