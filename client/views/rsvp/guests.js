Template.guests_list.rendered = function () {
  var height = window.innerHeight || document.body.clientHeight;

  this.find('ul.guests').style.height = height+'px';
};
Template.guests_list.helpers({
  nGuests: function () {
    if (typeof this.guests === 'undefined') return 0;

    return this.guests.count();
  }
});
Template.guests_list.events({
  'keyup .search input': function (e) {
    var query = e.currentTarget.value;

    Session.set('query', query);
  },
  'click #new-guest': function () {
    Session.set('selectedGuest', 'new');
  }
});

Template.guests_list_row.helpers({
  isUser: function () {
    return (typeof this.userId !== 'undefined') ? this.userId : false;
  },
  isActiveGuest: function () {
    return (this._id == Session.get('selectedGuest')) ? 'active' : '';
  },
  newsletter: function () {
    return (typeof this.newsletter !== 'undefined') ? true : false;
  }
});
Template.guests_list_row.events({
  'click .guest a.name': function (e) {
    $(e.currentTarget.parentNode.parentNode.parentNode.children).removeClass('active');
    $(e.currentTarget.parentNode.parentNode).addClass('active');
    Session.set('selectedGuest', this._id);
    Session.set('editMode', '');
    Session.set('guestNewsletterInfo', false);
  }
});

Template.guests_wrapper.helpers({
  newGuest: function () {
    return Session.equals('selectedGuest', 'new');
  }
});

Template.guests_info.helpers({
  isUser: function () {
    return (typeof this.guest.userId !== 'undefined') ? this.guest.userId : false;
  }
});
Template.guests_info.events({
  'click #newsletter-signup': function (e, tmpl) {
    Meteor.call('guestSubscribeNewsletter',
      {
        email: {email: this.guest.profile.email},
        merge_vars: {
          EMAIL: this.guest.profile.email,
          FNAME: this.guest.profile.first_name,
          LNAME: this.guest.profile.last_name,
          mc_language: this.guest.profile.locale.substring(0, 2)
        },
        double_optin: false,
        update_existing: true
      },
      function (error, result) {
        if (error) {
          logger.error(error);
        } else {
          logger.info('[MAILCHIMP] '+result);
        }
      }
    );
  }
});

Template.guest_profile_details.helpers({
  language: function () {
    if (typeof this.guest.profile.locale === 'undefined') return 'no language';

    if (this.guest.profile.locale.substring(0, 2) === 'pl') return 'Polish';
    if (this.guest.profile.locale.substring(0, 2) === 'es') return 'Spanish';
    if (this.guest.profile.locale.substring(0, 2) === 'en') return 'English';

    return 'no language';
  }
});
Template.guest_profile_details.events({
  'click #edit-mode': function (e, tmpl) {
    Session.set('editMode', 'guests-profile-details');
  }
});

Template.guest_profile_edit.helpers({
  selectedLang: function (value) {
    if ((this.guest && this.guest.profile.locale)
        && this.guest.profile.locale.substring(0, 2) === value) {
      return 'selected';
    } else {
      return '';
    }
  },
  selectedGender: function (value) {
    if ((this.guest && this.guest.profile.locale)
        && this.guest.profile.gender === value) {
      return 'checked'
    } else {
      return '';
    }
  }
});
Template.guest_profile_edit.events({
  'click #edit-mode': function (e, tmpl) {
    Session.set('editMode', '');
  },
  'click #save': function (e, tmpl) {
    var $first_name = tmpl.find('input#firstname'),
        $last_name = tmpl.find('input#lastname'),
        $email = tmpl.find('input#email'),
        $gender = tmpl.find('input[name=gender]:checked'),
        $locale = tmpl.find('select#locale')
        doc = {
          name: $first_name.value + ' ' + $last_name.value,
          first_name: $first_name.value,
          last_name: $last_name.value,
          email: $email.value,
          gender: ($gender === null) ? 'none' : $gender.value,
          locale: $locale.value
        };
    if (doc.email === '') delete doc.email;
    if (!validate.email($email.value) && $email.value !== '') {
      $email.parentNode.className += ' has-error';
      bootbox.alert('Not valid email! Please fill in a valid email or leave it empty.');
      return false;
    }

    if (this.guest) {
      Guests.update(this.guest._id, {$set: {profile: doc, modifiedAt: new Date()}}, function (error, nDocs) {
        if (error) {
          logger.info('Error: [' + error.reason + '] while editing guest by user ['+Meteor.userId()+']');
          logger.error(error);
        } else if (nDocs == 0) {
          logger.info('Error: [' + error.reason + '] while editing guest by user ['+Meteor.userId()+'] | No errors returned but any document modified in the DB');
        } else {
          logger.info('Guest edited successfuly by user ['+Meteor.userId()+']. '+nDocs+' documents modified in the DB.');
        }
      });
    } else {
      var newGuestDoc = {
        profile: doc,
        createdAt: new Date(),
        modifiedAt: new Date()
      }
      Guests.insert(newGuestDoc, function (error, guestId) {
        if (error) {
          logger.info('Error: [' + error.reason + '] while creating new guest by user ['+Meteor.userId()+']');
          logger.error(error);
        } else {
          logger.info('New guest ['+guestId+'] created successfuly by user ['+Meteor.userId()+'].');
          Session.set('selectedGuest', guestId);
        }
      });
    }
    Session.set('editMode', '');
  },
  /**
   * continue validation on every keyup or focusout event
   */
  'keyup input, focusout input': function (e, tmpl) {
    var inputType = e.currentTarget.attributes.type.value;
    var inputValue = e.currentTarget.value;

    if (!inputValue) {
      return false;
    } else if (validate[inputType] && validate[inputType](inputValue)) {
      e.currentTarget.parentNode.className = 'col-sm-10';
    } else {
      e.currentTarget.parentNode.className += ' has-error';
    }
  }
});

Template.guest_newsletter_details.helpers({
  newsletter: function () {
    return (typeof this.guest.newsletter !== 'undefined') ? this.guest.newsletter.mailchimp : false;
  },
  mailchimpInfo: function () {
    if (!Session.get('guestNewsletterInfo')) return false;

    return JSON.stringify(Session.get('guestNewsletterInfo').data[0], null, '<br>');
  }
});
Template.guest_newsletter_details.events({
  'click #newsletter-data': function (e, tmpl) {
    if (typeof this.guest.newsletter === 'undefined') return false;

    Meteor.call('newsletterMemberInfo',
      {
        emails: [ { euid: this.guest.newsletter.mailchimp.euid } ]
      },
      function (error, result) {
        var html = '';
        if (error) {
          logger.error(error);
        } else {
          Session.set('guestNewsletterInfo', result);
        }
      }
    );
  }
});
