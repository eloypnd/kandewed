Template.header.events({
  'click .glyphicon-chevron-down': function (e, tmpl) {
    $('.wrapper > section:nth-of-type(1)').smoothScroll();
    GAnalytics.event("User action", "click", 'arrow down');
  }
});
