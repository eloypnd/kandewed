Template.navbar.helpers({
  username: function() {
    return Meteor.user() && Meteor.user().profile.first_name;
  },
  profile_img: function() {
    if (Meteor.user().profile.facebook !== undefined) {
      return 'https://graph.facebook.com/' + Meteor.user().profile.facebook.username + '/picture';
    } else {
      return 'profile_default.jpg';
    }
  },
  language: function () {
    return i18n.getLang();
  },
  languages: function () {
    var lang = i18n.getLang();
    var languages = [];
    ['es', 'en', 'pl'].forEach(function (element) {
      if (element !== lang) {
        languages.push({lang: element});
      }
    })
    return languages;
  }
});

Template.navbar.events({
  'click a[href^="#"]': function (e, tmpl) {
    e.preventDefault();
    var target = e.target.attributes.href.value;

    $(target).smoothScroll();

    GAnalytics.event("User action", "click", target);
  },
  'click .navbar-lang a.btn-lang': function (e, tmpl) {
    var lang = e.target.attributes.lang.value;
    i18n.setLocale(lang);
    // TODO: veeeery ugly
    // [issue #6](https://bitbucket.org/eloypineda/kandewed/issue/6/no-force-refresh-after-change-language)
    window.location.reload();

    GAnalytics.event("User action", "change language", lang);
  },
  'click a.btn-logout': function (e, tmpl) {
    Meteor.logout();
  }
});

Template.navbar.rendered = function () {
  // set up change sections on scroll
  $('section.block').waypoint(function (direction) {
    var target = $(this).attr('id');

    GAnalytics.event("User action", "scroll to", target);

    $('ul.navbar-center li').removeClass('active');

    if (typeof target !== 'undefined') {
      var active = 'a[href^=#'+target+']';
      $(active).parent().addClass('active');
    }

  }, {offset: 50});
  // set active the first menu element
  $('ul.navbar-center li').removeClass('active');
  // set active language in navbar
  // $('ul.navbar-lang button[value='+i18n.language+']').parent().addClass('active');
}
