Template.layout.helpers({
  pageName: function(){
    var path = document.location.pathname;
    var route = Router.routes.filter(function (elem) {
      //if (elem.originalPath === path) {
      if (elem.re.test(path)) {
        return elem
      }
    });
    return route[0].name.replace('/', '-');
  },
  routeGroup: function () {
    var path = document.location.pathname;
    var route = Router.routes.filter(function (elem) {
      //if (elem.originalPath === path) {
      if (elem.re.test(path)) {
        return elem
      }
    });
    return route[0].options.group;
  }
});
