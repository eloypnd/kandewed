/**
 * header styles
 */
header.container {
  color: white;
  height: 100%;
  position: relative;

  &>.invitation {
    bottom: 1em;
    position: absolute;
    width: 100%;

    .glyphicon-chevron-down {
      background: rgba(0, 0, 0, 0.6);
      padding: 22px 16px 16px 16px;
      border-radius: 26px;
      margin: 16px 0 0;
    }
  }

  .pretext {
    font-family: 'Arvo', 'Footlight Light', Georgia, serif;
    font-size: 1.8em;
    margin: 40px 0;
    text-align: center;
  }
  h1 {
    font-family: 'Roboto', 'Helvetica Neue', Arial, sans-serif;
    font-size: 3.7em;
    font-weight: 700;
    margin: 0;
    text-align: center;
  }
  .wedline, .dateline {
    font-family: 'Arvo', 'Footlight Light', Georgia, serif;
    text-align: center;
    &:before, &:after {
      border-top: 2px solid white;
      content: " ";
      display: inline-block;
      width: 26%;
    }
    span {
      padding: 0 10px;
    }
  }
  .dateline {
    font-family: 'Habibi', Georgia, serif;
    &:before, &:after {
      width: 5%;
    }
  }
}

/* RWD */
@media only screen and (min-width: 480px) {
  /* 480 =================================================== */
  header.container {

    h1 {
      font-size: 6em;
    }
    .wedline, .dateline {
      &:before, &:after {
        width: 32%;
      }
    }
    .dateline {
      &:before, &:after {
        width: 21%;
      }
    }
  }
}
@media only screen and (min-width: 600px) {
  /* 600 =================================================== */
  header.container {

    h1 {
      font-size: 8em;
    }
    .wedline, .dateline {
      &:before, &:after {
        width: 37%;
      }
    }
    .dateline {
      &:before, &:after {
        width: 27%;
      }
    }
  }
}
@media only screen and (min-width: 768px) {
  /* 768 =================================================== */
  header.container {

    h1 {
      font-size: 10em;
    }
    .wedline, .dateline {
      &:before, &:after {
        width: 40%;
      }
    }
    .dateline {
      &:before, &:after {
        width: 32%;
      }
    }
  }
}
@media only screen and (min-width: 992px) {
  /* 992 =================================================== */
  header.container {

    h1 {
      font-size: 13em;
    }
    .wedline, .dateline {
      &:before, &:after {
        width: 42%;
      }
    }
    .dateline {
      &:before, &:after {
        width: 36%;
      }
    }
  }
}
@media only screen and (min-width: 1200px) {
  /* 1200 =================================================== */
  header.container {

    .pretext {
      font-size: 2em;
    }
    h1 {
      font-size: 15em;
      margin: 0 0 9px 0;
    }
    .wedline, .dateline {
      font-size: 1.6em;
      &:before, &:after {
        width: 42%;
      }
    }
    .dateline {
      &:before, &:after {
        width: 34%;
      }
    }
  }
}
