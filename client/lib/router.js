Router.configure({
  layoutTemplate: 'layout',
  yieldTemplates: {
    'background': {to: 'background'},
    'navbar'    : {to: 'navbar'},
    'header'    : {to: 'header'},
    'footer'    : {to: 'footer'}
  },
  notFoundTemplate: 'not_found'
});

/**
 * filters for the routes
 *
 * filters to apply to gruop of routes
 */
var filters = {

  isLoggedIn: function() {
    if (!(Meteor.loggingIn() || Meteor.user())) {
      logger.notice('Please Log In First.');
      this.redirect('/login');
      logger.route();
    }
  },

  isAdmin: function() {
    if(!Meteor.loggingIn() && !Roles.userIsInRole(Meteor.userId(), 'admin')) {
      logger.notice('Sorry, you don\'t have permissons to access to this page.');
      //
      delete this.yieldTemplates.admin_navbar;
      this.yieldTemplates.background = {to: 'background'};
      this.yieldTemplates.header = {to: 'header'};
      this.yieldTemplates.navbar = {to: 'navbar'};
      //
      this.redirect('home');
      logger.route();
      //TODO not force redirect
      //window.location.reload();
    }
  }

}
Router.onBeforeAction(filters.isLoggedIn, {only: [
   'home'
  , 'profile'
  , 'admin'
  , 'admin/users'
  , 'admin/guests'
  , 'admin/content'
  , 'admin/content/edit'
]});
Router.onBeforeAction(filters.isAdmin, {only: [
    'admin'
  , 'admin/users'
  , 'admin/guests'
  , 'admin/content'
  , 'admin/content/edit'
]});

/**
 * grouping routes
 *
 * using a filter we can setup some stuff for some group of routes
 */
var routeGroups = {

  logout: function () {
    this.router.configure({
      yieldTemplates: {
        'background_logout': {to: 'background'},
        'header'    : {to: 'header'}
      }
    });
    //this.clearRegion('background');
    //this.clearRegion('navbar');
    //this.clearRegion('footer');

    //this.setRegion('background', 'background_logout');

    //delete this.yieldTemplates.background;
    //delete this.yieldTemplates.navbar;
    //delete this.yieldTemplates.footer;

    //this.yieldTemplates.background_logout = {to: 'background'};

    this.route.group = 'logout';
  },
  login: function () {
    this.route.group = 'login';
  },
  admin: function () {
    this.router.configure({
      yieldTemplates: {
        'admin_navbar': {to: 'background'},
        'footer': {to: 'footer'}
      }
    });
    //delete this.yieldTemplates.background;
    //delete this.yieldTemplates.navbar;
    //delete this.yieldTemplates.header;

    //this.yieldTemplates.admin_navbar = {to: 'navbar'};

    this.route.group = 'admin';
  }
}
Router.onBeforeAction(routeGroups.logout, {only: ['login', 'signin']});
Router.onBeforeAction(routeGroups.login, {only: ['home']});
Router.onBeforeAction(routeGroups.admin, {only: [
    'admin'
  , 'admin/users'
  , 'admin/guests'
  , 'admin/content'
  , 'admin/content/edit'
]});

// Logging
Router.onRun(logger.route);

// Content collection subscription
Router.configure({
  waitOn: function () {
    // add here subscription to content
    var strigIsAdmin = this.path.split('/')[1] === 'admin' ? true : false;
    // don't subscribe when we are in admin
    if (!strigIsAdmin) {
      var path = this.path.split('#')[0];
      return Meteor.subscribe('pageContent', path);
    }
  }
});

Router.map(function () {
  /**
   * front-end routes
   *
   * @route /         home
   * @route /login    login
   * @route /signin   sigin
   *
   * @route /profile  profile
   * @route /rsvp     rsvp
   */
  this.route('home', {
    path: '/',
    group: 'login'
  });
  this.route('loading', {
    onBeforeAction: function () {
      //delete this.yieldTemplates.background;
      //delete this.yieldTemplates.navbar;
      //delete this.yieldTemplates.footer;
      //delete this.yieldTemplates.header;
    }
  });
  //this.route('not_auth');
  this.route('rsvp');
  this.route('profile');
  this.route('login', {
    group: 'logout'
  });
  this.route('signin', {
    group: 'logout'
  });

  /**
   * admin routes
   *
   * @route /admin
   * @route /admin/users
   * @route /admin/guests
   * @route /admin/content
   * @route /admin/content/:_id/edit
   */
  this.route('admin', {
    template: 'admin/dashboard',
    group: 'admin'
  });
  this.route('admin/users', {
    group: 'admin',
    waitOn: function () {
      return Meteor.subscribe('allUsers');
    },
    data: function () {
      var query = Session.get('query'),
          filter = {},
          order = {},
          userId = Session.get('selectedUser');
      var user = (userId) ? Meteor.users.findOne({_id: userId}) : false;

      if (query) {
        filter = {
          $or: [
            { 'profile.name': { $regex: query, $options: 'i' } },
            { 'profile.email': { $regex: query, $options: 'i' } }
          ]
        };
        order = { sort: { 'createdAt': -1 } };
      }

      return {
        users: Meteor.users.find(filter, order),
        user: user
      }
    }
  });
  this.route('admin/guests', {
    group: 'admin',
    waitOn: function () {
      return Meteor.subscribe('allGuests');
    },
    data: function () {
      var query = Session.get('query'),
          filter = {},
          order = {},
          guestId = Session.get('selectedGuest');
      var guest = (guestId) ? Guests.findOne({_id: guestId}) : false;

      if (query) {
        filter = {
          $or: [
            { 'profile.name': { $regex: query, $options: 'i' } },
            { 'profile.email': { $regex: query, $options: 'i' } }
          ]
        };
        order = { sort: { 'createdAt': -1 } };
      }

      return {
        guests: Guests.find(filter, order),
        guest: guest
      }
    }
  });
  this.route('admin/content', {
    group: 'admin',
    waitOn: function () {
      return Meteor.subscribe('allContent');
    },

    data: function () {
      return {
        content: Content.find({})
      }
    }
  });
  this.route('admin/content/edit', {
    path: 'admin/content/:_id/edit',
    group: 'admin',
    waitOn: function () {
      return Meteor.subscribe('singleContent', this.params._id);
    },
    data: function() {
      return {
        contentId: this.params._id,
        content: Content.findOne()
      }
    },
    action: function () {
      if (this.ready()) {
        this.render();
      } else {
        this.render('loading');
      }
    }
  });

});
