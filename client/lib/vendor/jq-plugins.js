/**
 * scroll to a element
 *
 * used to scroll to section when clicked in nav
 */
jQuery.fn.smoothScroll = function() {
  var target = '#' + this.attr('id');
  $('html, body').animate(
    {
      'scrollTop': this.offset().top
    }, 1200, 'easeInOutCirc', function () {
      window.location.hash = target;
    }
  );
};
